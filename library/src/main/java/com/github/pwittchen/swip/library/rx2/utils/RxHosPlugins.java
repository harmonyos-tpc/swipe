/**
 * Copyright (C) 2016 Piotr Wittchen
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.github.pwittchen.swip.library.rx2.utils;

import io.reactivex.Scheduler;
import io.reactivex.exceptions.Exceptions;
import io.reactivex.functions.Function;

import java.util.concurrent.Callable;

/**
 * 调度器plugin
 */
public class RxHosPlugins {

    private static volatile Function<Callable<Scheduler>, Scheduler> onInitMainThreadHandler;
    private static volatile Function<Scheduler, Scheduler> onMainThreadHandler;

    /**
     * 设置线程调度方法
     * @param handler 线程调度器方法
     */
    public static void setInitMainThreadSchedulerHandler(Function<Callable<Scheduler>, Scheduler> handler) {
        onInitMainThreadHandler = handler;
    }

    /**
     * 初始化调度器
     *
     * @param scheduler 线程调度器回调
     * @return 返回线程调度
     */
    public static Scheduler initMainThreadScheduler(Callable<Scheduler> scheduler) {
        if (scheduler == null) {
            throw new NullPointerException("scheduler == null");
        }
        Function<Callable<Scheduler>, Scheduler> f1 = onInitMainThreadHandler;
        if (f1 == null) {
            return callRequireNonNull(scheduler);
        }
        return applyRequireNonNull(f1, scheduler);
    }

    /**
     * 线程调度回调
     * @param handler 设置调度器
     */
    public static void setMainThreadSchedulerHandler(Function<Scheduler, Scheduler> handler) {
        onMainThreadHandler = handler;
    }

    /**
     * 初始化调度
     * @param scheduler 线程调度回调
     * @return 返回调度器
     */
    public static Scheduler onMainThreadScheduler(Scheduler scheduler) {
        if (scheduler == null) {
            throw new NullPointerException("scheduler == null");
        }
        Function<Scheduler, Scheduler> f1 = onMainThreadHandler;
        if (f1 == null) {
            return scheduler;
        }
        return apply(f1, scheduler);
    }

    /**
     * Removes all handlers and resets the default behavior.
     */
    public static void reset() {
        setInitMainThreadSchedulerHandler(null);
        setMainThreadSchedulerHandler(null);
    }

    static Scheduler callRequireNonNull(Callable<Scheduler> s1) {
        try {
            Scheduler scheduler = s1.call();
            if (scheduler == null) {
                throw new NullPointerException("Scheduler Callable returned null");
            }
            return scheduler;
        } catch (Throwable ex) {
            throw Exceptions.propagate(ex);
        }
    }

    static Scheduler applyRequireNonNull(Function<Callable<Scheduler>, Scheduler> f1, Callable<Scheduler> s1) {
        Scheduler scheduler = apply(f1, s1);
        if (scheduler == null) {
            throw new NullPointerException("Scheduler Callable returned null");
        }
        return scheduler;
    }

    static <T, R> R apply(Function<T, R> f1, T t1) {
        try {
            return f1.apply(t1);
        } catch (Throwable ex) {
            throw Exceptions.propagate(ex);
        }
    }

    private RxHosPlugins() {
        throw new AssertionError("No instances.");
    }
}
