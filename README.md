﻿# Swip
实现页面滑动的监听，其中包含普通实现和rxjava实现

## 主要特性
 * 支持4个方向的滑动监听
 * 支持rxjava代码实现
 

## 演示

|滑动监听实现 |
|:---:|
|<img src="screenshot/commonshow.gif" width="75%"/>|

 ## entry运行要求
 通过DevEco studio,并下载openHarmonySDK
 将项目中的build.gradle文件中dependencies→classpath版本改为对应的版本（即你的IDE新建项目中所用的版本）
 
 ## 集成
 
 ```
方式一：
通过library生成har包，添加har包到libs文件夹内
在entry的gradle内添加如下代码
implementation fileTree(dir: 'libs', include: ['*.jar', '*.har'])

方式二：
allprojects{
    repositories{
        mavenCentral()
    }
}
implementation 'io.openharmony.tpc.thirdlib:swipe:1.0.5'
 ```
 
## 普通使用
**Step 1**: Create `Swipe` attribute in the `Ability`:

```
swipe = new Swipe();
```

**Step 2**: Initialize `Swipe` object and set listener:

```
 swipe.setListener(new SwipeListener() {
             @Override public void onSwipingLeft(final TouchEvent event) {
                 info.setText("SWIPING_LEFT");
             }
 
             @Override public boolean onSwipedLeft(final TouchEvent event) {
                 info.setText("SWIPED_LEFT");
                 return false;
             }
 
             @Override public void onSwipingRight(final TouchEvent event) {
                 info.setText("SWIPING_RIGHT");
             }
 
             @Override public boolean onSwipedRight(final TouchEvent event) {
                 info.setText("SWIPED_RIGHT");
                 return false;
             }
 
             @Override public void onSwipingUp(final TouchEvent event) {
                 info.setText("SWIPING_UP");
             }
 
             @Override public boolean onSwipedUp(final TouchEvent event) {
                 info.setText("SWIPED_UP");
                 return false;
             }
 
             @Override public void onSwipingDown(final TouchEvent event) {
                 info.setText("SWIPING_DOWN");
             }
 
             @Override public boolean onSwipedDown(final TouchEvent event) {
                 info.setText("SWIPED_DOWN");
                 return false;
             }
         });
```

## rxjava使用
**Step 1**: Create `Swipe` attribute and `Subscription` in the `Ability`:

```
private Swipe swipe;
private Disposable disposable;
swipe = new Swipe();
```

**Step 2**: Initialize `Swipe` object and subscribe `Observable`:

```

   disposable = swipe.observe()
                 .subscribeOn(Schedulers.computation())
                 .observeOn(HarmonySchedulers.mainThread())
                 .subscribe(swipeEvent -> info.setText(swipeEvent.toString()));

```

`SwipeEvent` is an enum with the following values:

```
public enum SwipeEvent {
  SWIPING_LEFT,
  SWIPED_LEFT,
  SWIPING_RIGHT,
  SWIPED_RIGHT,
  SWIPING_UP,
  SWIPED_UP,
  SWIPING_DOWN,
  SWIPED_DOWN
}
```

**Step 3**: override `onTouchEvent(Component component, TouchEvent touchEvent)`:

```
   @Override
              public boolean onTouchEvent(Component component, TouchEvent touchEvent) {
                  swipe.dispatchTouchEvent(touchEvent);
                  return true;
  
              }
```

**Step 4**: dispose previously created `Disposable` when it's no longer needed:

```
   @Override
      protected void onInactive() {
          super.onInactive();
          safelyUnsubscribe(disposable);
      }
  
      private void safelyUnsubscribe(Disposable disposable) {
          if (disposable != null && !disposable.isDisposed()) {
              disposable.dispose();
          }
      }
```

## License

```
Copyright 2016 Piotr Wittchen

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
```